## gcloud 

## configuration management

https://cloud.google.com/sdk/docs/configurations  
https://cloud.google.com/sdk/gcloud/reference/config


get current setup
```
$ gcloud config list

# output example:
[compute]
region = europe-west3
zone = europe-west3-a
[core]
account = matteo@getanewsletter.com
disable_usage_reporting = True
project = gan-staging-296609

Your active configuration is: [gan-production]
```

set a configuration example
```
gcloud config set project gan-staging-296609 

output exaxmple
      Credentialed Accounts
ACTIVE  ACCOUNT
        bitsurs@gmail.com
*       matteo@getanewsletter.com

```


get current authorization (logged user)
```
gcloud auth list
```

set account
```
gcloud config set account bitsurs@gmail.com
```

configuration management
```
$ gcloud config configurations activate
$ gcloud config configurations create
$ gcloud config configurations delete
$ gcloud config configurations describe
$ gcloud config configurations list

```
